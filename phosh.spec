%global gvc_commit ae1a34aafce7026b8c0f65a43c9192d756fe1057

Name:           phosh
Version:        0.5.0
Release:        1%{?dist}
Summary:        Graphical shell for mobile devices

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/phosh
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/phosh/-/archive/v%{version}/%{name}-v%{version}.tar.gz

	
# This library doesn't compile into a DSO or ever has had any releases.
# Other projects, such as gnome-shell use it this way.
%undefine _disable_source_fetch
Source1:        https://gitlab.gnome.org/GNOME/libgnome-volume-control/-/archive/%{gvc_commit}/libgnome-volume-control-%{gvc_commit}.tar.gz
Source2:        phosh
Source3:        phosh.service

Patch0:         001-meson_options.patch
Patch1:         0001-phosh-showing-unlock-page-re-orients-the-screen.patch
Patch2:         0002-phosh-system-prompt-allow-blank-passwords.patch
Patch3:         0003-phosh-flashlight-torch.patch

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pam-devel
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(gcr-3) >= 3.7.5
BuildRequires:  pkgconfig(gio-2.0) >= 2.56
BuildRequires:  pkgconfig(gio-unix-2.0) >= 2.50.0
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gnome-desktop-3.0) >= 3.26
BuildRequires:  pkgconfig(gobject-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.22
BuildRequires:  pkgconfig(gtk+-wayland-3.0) >= 3.22
BuildRequires:  pkgconfig(libhandy-1)
BuildRequires:  pkgconfig(libnm) >= 1.14
BuildRequires:  pkgconfig(libpulse) >= 2.0
BuildRequires:  pkgconfig(libpulse-mainloop-glib)
BuildRequires:  pkgconfig(polkit-agent-1) >= 0.105
BuildRequires:  pkgconfig(upower-glib) >= 0.99.1
BuildRequires:  pkgconfig(wayland-client) >= 1.14
BuildRequires:  pkgconfig(wayland-protocols) >= 1.12
BuildRequires:  pkgconfig(libfeedback-0.0)
BuildRequires:  feedbackd-devel
BuildRequires:  pkgconfig(libsecret-1)
BuildRequires:  at-spi2-core
BuildRequires:  xvfb-run
BuildRequires:  xauth
BuildRequires:  desktop-file-utils
BuildRequires:  git
BuildRequires:  phoc >= 0.4.0
BuildRequires:  Mesa-libGL1
BuildRequires:  Mesa-libGL-devel
BuildRequires:  Mesa-libEGL1
BuildRequires:  Mesa-libEGL-devel
BuildRequires:  Mesa-dri
BuildRequires:  Mesa-dri-devel
BuildRequires:  mutter
BuildRequires:  gnome-themes-extras
BuildRequires:  pkgconfig(gsettings-desktop-schemas)
BuildRequires:  gtk3-devel
BuildRequires:  libgcr-devel
BuildRequires:  libgnome-desktop-3-devel
BuildRequires:  gsettings-backend-dconf
BuildRequires:  gnome-session


Requires:       gnome-session
Requires:       lato-fonts

%description
Phosh is a simple shell for Wayland compositors speaking the layer-surface
protocol. It currently supports

* a lockscreen
* brightness control and nighlight
* the gcr system-prompter interface
* acting as a polkit auth agent
* enough of org.gnome.Mutter.DisplayConfig to make gnome-settings-daemon happy
* a homebutton that toggles a simple favorites menu
* status icons for battery, wwan and wifi


%prep
%setup -a1 -q -n %{name}-v%{version}
%patch0 -p0
%patch1 -p1
%patch2 -p1
%patch3 -p1
for i in $(find debian/patches/ -name "*.patch"); do patch -p1 < $i; done
rmdir subprojects/gvc
ln -s ../libgnome-volume-control-%{gvc_commit} subprojects/gvc
cd subprojects/gvc
for i in $(find debian/patches/ -name "*.patch"); do patch -p1 < $i; done

%build
%meson
%meson_build


%install
mkdir -p $RPM_BUILD_ROOT/etc/pam.d/
cp %{SOURCE2} $RPM_BUILD_ROOT/etc/pam.d/
mkdir -p $RPM_BUILD_ROOT/etc/systemd/system/
cp %{SOURCE3} $RPM_BUILD_ROOT/etc/systemd/system/phosh.service
%meson_install
%find_lang %{name}

%post
%systemd_post phosh.service
systemctl enable phosh.service

%preun
%systemd_preun posh.service


%postun
%systemd_postun_with_restart phosh.service

%check
desktop-file-validate \
        %{buildroot}/%{_datadir}/applications/sm.puri.Phosh.desktop
LC_ALL=C.UTF-8 xvfb-run sh <<'SH'
%meson_test
SH


%files -f %{name}.lang
%{_bindir}/phosh
%{_bindir}/phosh-osk-stub
%{_libexecdir}/phosh
%{_datadir}/applications/sm.puri.Phosh.desktop
%{_datadir}/glib-2.0/schemas/sm.puri.phosh.gschema.xml
%{_datadir}/glib-2.0/schemas/sm.puri.phosh.enums.xml
%dir %{_datadir}/gnome-session/
%dir %{_datadir}/gnome-session/sessions
%{_datadir}/gnome-session/sessions/phosh.session
%{_datadir}/applications/sm.puri.OSK0.desktop
%dir %{_datadir}/wayland-sessions/
%{_datadir}/wayland-sessions/phosh.desktop
%{_datadir}/phosh
%{_sysconfdir}/pam.d/phosh
%{_sysconfdir}/systemd/system/phosh.service
%doc README.md
%doc debian/phosh.service
%license COPYING


%changelog

